# <!-- coding: utf-8 -->
#
# quelques fonction pour les ika
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
mga  <- function() {
  source("geo/scripts/ika.R");
}

#
# les commandes permettant le lancement
Drive <<- substr( getwd(),1,2)
baseDir <<- sprintf("%s/web", Drive)
cbnbrestDir <- sprintf("%s/bvi35/CouchesCBNBrest", Drive);
ignDir <<- sprintf("%s/bvi35/CouchesIGN", Drive);
varDir <<- sprintf("%s/bvi35/CouchesIKA", Drive);
osoDir <<- sprintf("%s/bvi35/CouchesOSO", Drive);
cfgDir <<- sprintf("%s/web/geo/IKA", Drive)
texDir <<- sprintf("%s/web/geo/IKA", Drive)
webDir <<- sprintf("%s/web.heb/bv/ika", Drive)
copernicusDir <- sprintf("%s/bvi35/CouchesCopernicus", Drive)

setwd(baseDir)
source("geo/scripts/mga.R");
source("geo/scripts/mga_gdal.R");
source("geo/scripts/misc.R");
source("geo/scripts/misc_carto.R");
source("geo/scripts/misc_couches.R");
source("geo/scripts/misc_drive.R");
source("geo/scripts/misc_fonds.R");
source("geo/scripts/misc_gdal.R");
source("geo/scripts/misc_geo.R");
source("geo/scripts/misc_ggplot.R");
source("geo/scripts/misc_grille.R");
source("geo/scripts/misc_mnhn.R");
source("geo/scripts/misc_ocs.R");
source("geo/scripts/misc_osm.R");
source("geo/scripts/misc_rpg.R");
source("geo/scripts/misc_sna.R");
source("geo/scripts/misc_tex.R");
source("geo/scripts/couches_wmts.R");
source("geo/scripts/ika_acp.R");
source("geo/scripts/ika_cartes.R");
source("geo/scripts/ika_couches.R");
source("geo/scripts/ika_drive.R");
source("geo/scripts/ika_donnees.R");
source("geo/scripts/ika_fonds.R");
source("geo/scripts/ika_matthieu.R");
source("geo/scripts/ika_ocs.R");
source("geo/scripts/ika_openrouteservice.R");
source("geo/scripts/ika_parcours.R");
source("geo/scripts/ika_troncons.R");
DEBUG <- FALSE
if ( interactive() ) {
  print(sprintf("ika.R"))
  graphics.off()
} else {
}
